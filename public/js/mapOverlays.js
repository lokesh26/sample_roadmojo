Ex_InfoBox.prototype = new google.maps.OverlayView();

	/** @constructor */
	function Ex_InfoBox(options, map) {

  // Initialize all properties.
  this.options_ = options;
  this.map_ = map;

  // Define a property to hold the text's div. We'll
  // actually create this div upon receipt of the onAdd()
  // method so we'll leave it null for now.
  this.div_ = null;

  // Explicitly call setMap on this overlay.
  this.setMap(map);
}

Ex_InfoBox.prototype.onAdd = function() {

	var div = document.createElement('div');
	div.style.position = 'absolute';
	div.className = "ex_infobox";

  // Create the img element and attach it to the div.
  var ib_text = document.createElement('div');
  ib_text.style.width = '100%';
  ib_text.style.height = '100%';
  ib_text.innerHTML = this.options_.content;
  div.appendChild(ib_text);

  this.div_ = div;

  // Add the element to the "overlayLayer" pane.
  var panes = this.getPanes();
  panes.floatPane.appendChild(div);
};

Ex_InfoBox.prototype.draw = function() {

  // We use the south-west and north-east
  // coordinates of the overlay to peg it to the correct position and size.
  // To do this, we need to retrieve the projection from the overlay.
  var overlayProjection = this.getProjection();

  // Retrieve the south-west and north-east coordinates of this overlay
  // in LatLngs and convert them to pixel coordinates.
  // We'll use these coordinates to resize the div.
  var coordinates = overlayProjection.fromLatLngToDivPixel(this.options_.latLng);

  // Resize the image's div to fit the indicated dimensions.
  var div = this.div_;
  div.style.left = coordinates.x + 18 + 'px';
  div.style.top = coordinates.y - 14 + 'px';
};

Ex_InfoBox.prototype.setContent = function (content) {
	this.textContainer.innerHTML(content);
}

// The onRemove() method will be called automatically from the API if
// we ever set the overlay's map property to 'null'.
Ex_InfoBox.prototype.onRemove = function() {
	this.div_.parentNode.removeChild(this.div_);
	this.div_ = null;
};

// Map icons - shades
var Markers = {
    whiteIcon: {
        'path': google.maps.SymbolPath.CIRCLE,
        'strokeWeight': 2,
        'strokeColor': "#000000",
        'fillColor': "#ffffff",
        'fillOpacity': 1,
        'scale': "5"
    },

    redIcon: {
        'path': google.maps.SymbolPath.CIRCLE,
        'strokeWeight': 2,
        'strokeColor': "#ffffff",
        'fillColor': "#fe5435",
        'fillOpacity': 1,
        'scale': "9"
    }
};