﻿// Roadmojo JavaScript

$(window).load(init);

function init() {
    // initialize foundation
    $(document).foundation();

    switch (window.rmojo.id) {
        case "feed":
        case "me_trips":
            me_trips.initialize();
            break;

        case "me_followingPlaces":
            me_followingPlaces.initialize();
            break;

        case "profile_edit":
            profile_edit.initialize();
            break;

        case "profile_changePassword":
            profile_changePassword.initialize();
            break;

        case "profile_notifications":
            profile_notifications.initialize();
            break;

        case "me_followingFindInvite":
            me_followingFindInvite.initialize();
            break;

        case "explore":
            explore.initialize();
            break;
    }

    common.initialize();
}

var utilities = {
    //Fill a HTML <select></select> with list of values in JavaScript array
    fillDropDown: function ($el, data, name, value) {
        for (var i = 0; i < data.length; i++) {
            /**
            * Fix #2: Updated method to create elements using jQuery, rather than raw HTML
            */
            $('<option/>').text(data[i][name]).attr('value', data[i][value]).appendTo($($el));
        }
    },

    //Fill a HTML <select></select> with list of values in JavaScript data object
    fillDropDown2: function ($el, data, nameKey, valueKey) {
        $.each(data, function (key, value) {
            if (valueKey) key = value[valueKey];
            if (nameKey) value = value[nameKey];
            $('<option/>').text(value).attr('value', key).appendTo($($el));
        });
    },

    //Method to create a HTML DOM element
    createElement: function (element, parent, attributes) {
        /**
        * @params
        *
        * element
        * Type: Selector
        * e.g. "<div/>"
        * String name of element which is required to be created
        *
        * parent
        * Type: String | Element
        * e.g. "#foo", $("#foo"), document.getElementById('foo')
        * Parent element of the newly created element
        *
        * attributes
        * Type: Object
        * e.g. {'className': 'bar', html: "Lorem Ipsum", id: "foo"}
        * Object of attributes to be applied to this element
        */

        if (!attributes) attributes = {};

        //create specified element
        var $el = $(element, attributes);

        //append this element to parent, if available
        if (parent) $el.appendTo($(parent));

        //return the newly created element
        return $el;
    },

    //Method to create JavaScript object of all fields in a <form></form>
    formToObject: function (form) {
        /*
        * Fix #3: Updated method to use jQuery rather that raw HTML
        */
        var $form = $(form),
        json = {};

        //Get Inputs
        $('input', $form).each(function (index, el) {
            var $el = $(el);

            //stay away, if element is disabled
            if ($el.is(":disabled")) return;

            name = $el.attr("name");
            switch ($el.attr("type")) {
                case 'checkbox':
                    //For checkboxes, we need to send values 0/1 rather than 'on', which is natively provided by HTML
                    $el.prop("checked") ? json[name] = "1" : json[name] = "0";
                    break;

                case 'radio':
                    if ($el.prop("checked")) json[name] = $el.val();
                    break;

                default:
                    json[name] = $el.val();
                    break;
            }
        });

        //Get Selects and textareas
        $('select,textarea', $form).each(function (index, el) {
            var $el = $(el);

            //stay away, if element is disabled
            if ($el.is(":disabled")) return;

            json[$el.attr("name")] = $el.val();
        });

        return json;
    },

    //Method for removal for multiple keys(in array) from a JavaScript Object
    dropKeysFromObject: function (obj, dropFields) {
        for (var i = 0; i < dropFields.length; i++) {
            if (obj[dropFields[i]]) delete obj[dropFields[i]];
        }

        return obj;
    },

    // load a javascript file from server
    loadJScript: function (fileName, callback) {
        var JSPATH = "/html/js/";

        $.getScript(JSPATH + fileName + ".js", callback);
    },

    //Method to load Google Maps API
    loadGMapScript: function (callback) {
        /*
        * Fix #4: Revised method to load Google Maps API - From Method 1 to Method 2
        * 
        * Google Maps API can be loaded using 2 methods
        * Method 1 creates a script tag and appends to document.body, e.g. $('<script/>').attr('src', "http://maps.googleapis.com/maps/api/js?sensor=false").appendTo(document.body);
        * Method 2 requests Google Maps API via a jQuery getScript method and calls all dependent methods henceforth
        */
        $.getScript("http://maps.googleapis.com/maps/api/js?sensor=false", function (data, textStatus, jqxhr) {
            if (callback) callback();
        });
    },

    /**
    * Create a new map, using Google Maps API, which should already be imported
    * Google Maps URL: http://maps.googleapis.com/maps/api/js?sensor=false
    */
    createGMap: function (el) {
        //if (typeof google.maps === "undefined") throw new Error("Google Maps required, but not imported.");

        var mapOptions = {
            zoom: 2,
            center: new google.maps.LatLng(27.219721, 78.019480),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            scrollwheel: false
        };

        //This method returns the Google Map object
        return new google.maps.Map(el, mapOptions);
    },

    /**
    * Set Autocomplete with Google Places
    * Google Maps with Place URL: http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places
    */
    setAutocompleteSearch: function (map, inputElement) {
        //if (typeof google.maps.places === "undefined") throw new Error("Google Places library required, but not imported.");

        var autocomplete = new google.maps.places.Autocomplete(inputElement);
        autocomplete.bindTo('bounds', map);

        return autocomplete;
    },

    /**
    * Get address of a location by its latitude and longitude. This method expects a callback which receives a formatted address string and address componets as params.
    * 
    * Requires Google Maps
    * URL: http://maps.googleapis.com/maps/api/js?sensor=false
    */
    getAddressByLatLng: function (latitude, longitude, callback) {
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({
            'location': new google.maps.LatLng(latitude, longitude)
        }, function (geocoderResult, geocoderStatus) {
            if (geocoderStatus == google.maps.GeocoderStatus.OK) {
                callback(geocoderResult[0].formatted_address, geocoderResult[0].address_components);
            } else {
                console.log("Google Geocoding Error: " + geocoderStatus);
            }
        });
    },

    //read photo from <input type="file" /> element($inputElement) and display it in <img src="" />($imageElement) tag
    readAndDisplayPhoto: function ($inputElement, $imageElement) {
        $inputElement = $($inputElement);

        if ($inputElement.prop('files') && $inputElement.prop('files')[0]) {
            var fileReader = new FileReader();
            fileReader.onload = function (ev) {
                $($imageElement).attr('src', ev.target.result);
            };
            fileReader.readAsDataURL($inputElement.prop('files')[0]);
        }
    },

    /*
    * this method uses an external plugin to set ellipsis
    * jquery.dot.dot.dot.min.js (https://github.com/FrDH/jQuery.dotdotdot)
    *
    * Provide a jQuery style selector to apply ellipsis
    */
    setdotdotdot: function (selector) {
        $(selector).dotdotdot({
            'ellipsis': "...",
            'wrap': "word",
            'fallbackToLetter': true,
            'after': null,
            'watch': true
        });
    },

    //convert JSON string to JavaScript object
    JSONtoObject: function (json) {
        if (JSON) return JSON.parse(json);
        else return eval('(' + json + ')');
    },

    //get cookie from browser, for key provided
    getCookieValue: function (key) {
        return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
    },

    //convert a data array to object mapper
    createMapFromArray: function (dataArray, key) {
        var mapper = {};
        if (!key) key = "id";
        for (var i = 0, l = dataArray.length; i < l; i++) {
            mapper[dataArray[i][key]] = dataArray[i];
        }

        return mapper;
    },

    //get number of keys in object
    getObjectLength: function (a) {
        return $.map(a, function (n, i) { return i; }).length;
    },

    //compare first string with second, upto n number of characters
    strncmp: function (a, b, n) {
        return a.substring(0, n) == b.substring(0, n);
    },

    // get parameter value from URL
    getURLVar: function (name) {
        return (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1];
    },

    /**
    * Hash a string using SHA256 algorithm
    * 
    * Requires sha256.min.js
    * Crypto-js http://code.google.com/p/crypto-js/
    *
    * returns SHA256 hash
    */
    sha256: function (strval) {
        if (!CryptoJS.SHA256) {
            console.log("Crypto library SHA256 not included!");
            return false;
        }

        return CryptoJS.SHA256(strval).toString(CryptoJS.enc.Hex);
    },

    // extract bits from a 'value' from 'start_pos' to 'end_pos'
    extractBits: function (value, start_pos, end_pos) {
        var mask = (1 << (end_pos - start_pos)) - 1;
        return (value >> start_pos) & mask;
    },

    // scroll smoothly to top of page
    scrollToTop: function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    }
};

var me_trips = {
    initialize: function () {

        // for drafts page
        if (utilities.getURLVar("_loc") === "drafts") {
            this._isDraftsPage = true;
            $("#roadtripTab").removeClass("current");
            $("#draftsTab").addClass("current");
        }

        // for other's profile page
        if (utilities.getURLVar("_loc") === "otherProfile") {
            this._isOtherProfilePage = true;

            //$("#roadtripTab").removeClass("current");
            $("#draftsTab,#activityTab,#recommendedTab").next().remove();
            $("#draftsTab,#activityTab,#recommendedTab").remove();
        }

        var sampleFeedData = [{
            'userPic': "../images/feed_photo.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "../images/coffee-beans.jpg",
            'mapLink': "",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "../images/feed_photo.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "../images/coffee-beans.jpg",
            'mapLink': "",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "../images/feed_photo.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "../images/coffee-beans.jpg",
            'mapLink': "",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "../images/feed_photo.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "../images/coffee-beans.jpg",
            'mapLink': "",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "../images/feed_photo.jpg",
            'userName': "John Doe",
            'minsAgo': "18",
            'noViews': "71",
            'tripPic': "",
            'mapLink': "http://maps.googleapis.com/maps/api/staticmap?center=-15.800513,-47.91378&zoom=15&size=600x450&sensor=false",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "15",
            'noMoments': "7",
            'noPhotos': "14",
            'likes': "5",
            'comments': "8"
        }];

        // sample feed printing
        this.printFeeds(sampleFeedData);
    },

    // Flag to indicate if current page is a drafts page
    _isDraftsPage: false,

    // Flag to indicate if current page is other's profile
    _isOtherProfilePage: false,

    // create a feed HTML
    createFeed: function (feedDetails) {
        var $feed = utilities.createElement("<div/>", null, { 'class': "feed" });

        var $head = utilities.createElement("<div/>", $feed, { 'class': "head clearfix" });
        var $headLink = utilities.createElement("<a/>", $head, { 'href': "profile_edit.html" });
        utilities.createElement("<div/>", $headLink, { 'class': "user-photo left", 'style': "background-image: url('" + feedDetails.userPic + "')" });
        utilities.createElement("<span/>", $headLink, { 'html': feedDetails.userName });
        utilities.createElement("<span/>", $head, { 'class': "views right", 'html': feedDetails.minsAgo + " mins ago &bull; " + feedDetails.noViews + " views" });

        var $photo = utilities.createElement("<div/>", $feed, { 'class': "photo" });
        utilities.createElement("<img/>", $photo, { 'src': feedDetails.tripPic || feedDetails.mapLink, 'alt': "photo" });
        var $overlay = utilities.createElement("<div/>", $photo, { 'class': "overlay" });

        if (this._isDraftsPage) utilities.createElement("<a/>", $overlay, { 'href': "#", 'class': "button button-edit tiny radius", 'html': "<span class='icon-edit'></span>&nbsp;&nbsp;EDIT & PUBLISH" });
        else utilities.createElement("<a/>", $overlay, { 'href': "#", 'class': "button button-edit tiny radius", 'html': "<span class='icon-edit'></span>&nbsp;&nbsp;EDIT" });

        var $deleteButton = utilities.createElement("<a/>", $overlay, { 'href': "#", 'class': "button button-delete tiny radius", 'html': '<span class="icon-delete"></span>', 'data-reveal-id': "confirmDeleteModal" });
        $deleteButton.on("click", function () {
            common._confirmsDelete = function () {
                //$feed.remove();
                $('.cols-wrapper').masonry('remove', $feed);
            };
        });

        var $content = utilities.createElement("<div/>", $feed, { 'class': "content" });
        utilities.createElement("<div/>", $content, { 'class': "trip-title", 'html': feedDetails.tripName });
        feedDetails.tripDesc && utilities.createElement("<p/>", $content, { 'html': feedDetails.tripDesc });

        utilities.createElement("<div/>", $feed, { 'class': "separator", 'html': "<hr />" });

        var $foot = utilities.createElement("<div/>", $feed, { 'class': "foot clearfix" });
        utilities.createElement("<span/>", $foot, { 'html': feedDetails.noMilestones + " milestones, " + feedDetails.noMoments + " moments, " + feedDetails.noPhotos + " photos" });

        if (!this._isDraftsPage) {
            var $right = utilities.createElement("<span/>", $foot, { 'class': "right" });
            utilities.createElement("<span/>", $right, { 'class': "like", 'html': '<span class="icon-like"></span>&nbsp;' + feedDetails.likes });
            utilities.createElement("<span/>", $right, { 'class': "comment", 'html': '<span class="icon-comment"></span>&nbsp;' + feedDetails.comments });
        }

        return $feed;
    },

    /**SAMPLE AJAX call**/
    // AJAX call to fetch list of feeds
    fetchData: function (fetchCallback) {
        var self = this;

        $.ajax({
            type: "GET",
            async: true,
            url: "",
            success: function (data, textStatus, jqXHR) {
                // data array should be received here in JSON format

                try {
                    // convert JSON data to JS array/object
                    data = utilities.JSONtoObject(data);
                } catch (ex) {
                    console.log("invalid list of feed received from server.");
                    return;
                }

                if (fetchCallback) fetchCallback.apply(self, [data]);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch list of feeds " + errorThrown);
            }
        });
    },

    // print list of feeds on page
    printFeeds: function (listOfFeeds, $container, feedClass) {
        var $feed = null,
    		l = listOfFeeds.length,
    		i = 0;

        if (!$container) $container = '.cols-wrapper';
        $container = $($container);

        for (; i < l; i++) {
            $feed = this.createFeed(listOfFeeds[i]);

            // Apply additional feed class if available
            if (feedClass) $feed.addClass(feedClass);

            $container.append($feed);
        }

        // masonrize feeds
        $container.imagesLoaded(function () {
            $container.masonry({
                // columnWidth: 160,
                "gutter": 18,
                itemSelector: '.feed'
            });

            $container.masonry('on', 'removeComplete', function (msnryInstance, removedItems) {
                $container.masonry();
            });
        });
    }
};
/*me_followingFindInvite*/
var me_followingFindInvite = {
    initialize: function () {
        var self = this;

        // Quick inits
        this.initInvite();
        this.getListOfFollowers(this.displayFollowers);

        // Currently Following Places

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        self.followingPeople = [
        { 'id': 1, 'name': "Mark Jenkins", 'desc': "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, perspiciatis...", 'roadtripsCount': "32", 'followingCount': "08", 'tripMiles': "89", 'isBeingFollowed': true },
        { 'id': 2, 'name': "Sudeep MP", 'desc': "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, perspiciatis...", 'roadtripsCount': "101", 'followingCount': "08", 'tripMiles': "89", 'isBeingFollowed': true },
        { 'id': 3, 'name': "Tushar", 'desc': "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, perspiciatis...", 'roadtripsCount': "22", 'followingCount': "08", 'tripMiles': "89", 'isBeingFollowed': true },
        { 'id': 4, 'name': "Robin", 'desc': "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, perspiciatis...", 'roadtripsCount': "12", 'followingCount': "08", 'tripMiles': "89", 'isBeingFollowed': true }
        ];

        //count the user being followed
        this.setFollowingPeopleCount();

        // If results are fetched successfully
        self.printRow(self.followingPeople);
    },

    followingPeople: [],

    followers: [],

    // Create a search results row
    _createResultRow: function (resultDetails, followCallback, unfollowCallback) {
        var self = this;
        var $row = utilities.createElement("<div/>", null, { 'class': "content-wrapper following-people" });
        var $rowContainer = utilities.createElement("<div/>", $row, { 'class': "row" });
        var $col1 = utilities.createElement("<div/>", $rowContainer, { 'class': "medium-2 large-1 columns" });
        var $col2 = utilities.createElement("<div/>", $rowContainer, { 'class': "medium-10 large-11 columns" });
        var $followingPeopleDetails = utilities.createElement("<div/>", $col2, { 'class': "following-people-details" });

        utilities.createElement("<div/>", $col1, { 'class': "user-photo" });
        utilities.createElement("<div/>", $followingPeopleDetails, { 'class': "following-people-name", 'html': resultDetails.name });
        utilities.createElement("<div/>", $followingPeopleDetails, { 'class': "following-people-desc", 'html': resultDetails.desc });
        utilities.createElement("<div/>", $followingPeopleDetails, { 'class': "following-people-desc", 'html': "<span>" + resultDetails.tripMiles + " Miles, </span>" + resultDetails.roadtripsCount + " <span>Roadtrips, </span><span>" + resultDetails.followingCount + " Followers</span>" });

        common.createFollowButton($col2, { 'data': resultDetails, 'method': followCallback }, { 'data': resultDetails, 'method': followCallback }, !resultDetails.isBeingFollowed, resultDetails.isBeingFollowed);

        return $row;
    },

    // Set number of people being followed
    setFollowingPeopleCount: function () {
        $("#followingPeopleCount").html(this.followingPeople.length);
    },

    /**
    * This method should be invoked, whenever this page is loaded
    * @params
    * searchResults - Array of search results/data result via ajax
    */
    printRow: function (listOfPeople) {
        var $resultsContainer = $("#followingContainer");

        for (var i = 0, l = listOfPeople.length; i < l; i++) {
            $resultsContainer.append(this._createResultRow(listOfPeople[i]));
        }
    },

    // Initialize invite section
    initInvite: function () {
        var self = this;

        $("#inviteForm").validate({
            highlight: function (element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function (form) {
                self.sendInvite();

                return false;
            }
        });
    },

    sendInvite: function () {
        var inviteData = utilities.formToObject("#inviteForm");

        // AJAX call goes here

        // If call completes succesfully an appropriate success/error message may be displayed
    },

    // Get list of followers from server - Tab 2
    getListOfFollowers: function (fetchCallback) {
        var self = this;

        // Fetch & display Currently Following Places

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        self.followers = [
        { 'id': 1, 'name': "Mark Jenkins", 'desc': "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, perspiciatis...", 'roadtripsCount': "32", 'followingCount': "08", 'tripMiles': "89", 'isBeingFollowed': true },
        { 'id': 2, 'name': "Sudeep MP", 'desc': "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, perspiciatis...", 'roadtripsCount': "101", 'followingCount': "08", 'tripMiles': "89", 'isBeingFollowed': true },
        { 'id': 3, 'name': "Tushar", 'desc': "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, perspiciatis...", 'roadtripsCount': "22", 'followingCount': "08", 'tripMiles': "89", 'isBeingFollowed': true },
        { 'id': 4, 'name': "Robin", 'desc': "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit, perspiciatis...", 'roadtripsCount': "12", 'followingCount': "08", 'tripMiles': "89", 'isBeingFollowed': true }
        ];

        // If results are fetched successfully
        fetchCallback && fetchCallback.apply(self, [self.followers]);
    },

    displayFollowers: function (listOfPeople) {
        var $toFollowContainer = $("#followersContainer"),
    		i = 0,
    		l = listOfPeople.length;

        for (; i < l; i++) {
            $toFollowContainer.append(this.createPersonRow(listOfPeople[i]));
        }

        this.setFollowersCount();
    },

    createPersonRow: function (personDetails) {
        var $person = utilities.createElement("<div/>", null, { 'class': "content-wrapper following-people" });
        var $row = utilities.createElement("<div/>", $person, { 'class': "row" });
        var $col = utilities.createElement("<div/>", $row, { 'class': "small-2 large-1 columns" });
        utilities.createElement("<div/>", $col, { 'class': "user-photo" });

        $col = utilities.createElement("<div/>", $row, { 'class': "small-10 large-11 columns" });
        var $temp = utilities.createElement("<div/>", $col, { 'class': "following-people-details" });
        utilities.createElement("<div/>", $temp, { 'class': "following-people-name", 'html': personDetails.name });
        utilities.createElement("<div/>", $temp, { 'class': "following-people-desc", 'html': personDetails.desc });
        utilities.createElement("<div/>", $temp, { 'class': "following-people-info", 'html': "<span>" + personDetails.tripMiles + " Miles, </span> <span>" + personDetails.roadtripsCount + " Roadtrips, </span> <span>" + personDetails.followingCount + " Followers</span>" });
        common.createFollowButton($col, { 'data': personDetails, 'method': null }, { 'data': personDetails, 'method': null }, true, false);

        return $person;
    },

    // Set number of followers
    setFollowersCount: function () {
        $("#followersCount").html(this.followers.length);
    },
};

var me_followingPlaces = {
    initialize: function () {
        var self = this;

        // Fetch & display Currently Following Places

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        self.followingPlaces = [
        { 'id': 1, 'location': "New Delhi, India", 'roadtripsCount': "32", 'followingCount': "08", 'isBeingFollowed': true },
        { 'id': 2, 'location': "Bangalore, India", 'roadtripsCount': "05", 'followingCount': "38", 'isBeingFollowed': true },
        { 'id': 3, 'location': "Mumbai, India", 'roadtripsCount': "2", 'followingCount': "14", 'isBeingFollowed': true },
        { 'id': 4, 'location': "Agra, India", 'roadtripsCount': "71", 'followingCount': "25", 'isBeingFollowed': true }
        ];

        // If results are fetched successfully
        this.setFollowingPlacesCount();
        self.searchComplete(self.followingPlaces, true);


        // Fetch & display Suggested Places

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        self.suggestedPlaces = [
        { 'id': 1, 'location': "New Delhi, India", 'roadtripsCount': "32", 'followingCount': "08", 'followersCount': "34", 'isBeingFollowed': false },
        { 'id': 2, 'location': "Bangalore, India", 'roadtripsCount': "05", 'followingCount': "38", 'followersCount': "34", 'isBeingFollowed': false },
        { 'id': 3, 'location': "Mumbai, India", 'roadtripsCount': "2", 'followingCount': "14", 'followersCount': "34", 'isBeingFollowed': false },
        { 'id': 4, 'location': "Agra, India", 'roadtripsCount': "71", 'followingCount': "25", 'followersCount': "34", 'isBeingFollowed': false }
        ];

        // If results are fetched successfully
        this.setSuggestedPlacesCount();
        self.suggetionsFound(self.suggestedPlaces, true);


        // Instant search for places
        $("#placesSearch").on("keyup", Foundation.utils.throttle(function (e) {
            var $searchKeywords = $(this).val();

            // Show all places before filtering
            $( "#searchResultsContainer .content-wrapper" ).show();

            $( "#searchResultsContainer .content-wrapper" ).filter(function( index ) {
                // Hide useless results, not containing the search term
                return $(this).find(".place .place-title").html().toLowerCase().indexOf($searchKeywords.toLowerCase()) == -1;
            }).hide();

        }, 300));

        // Further Search Results
        $("#searchFurther").on("click", function (ev) {
            ev.preventDefault();

            self.searchFurther();
        });

        // More suggestions
        $("#moreSuggestions").on("click", function (ev) {
            ev.preventDefault();

            self.moreSuggestions();
        });
    },

    followingPlaces: [],

    suggestedPlaces: [],

    // Set number of places being followed
    setFollowingPlacesCount: function () {
        $("#followingPlacesCount").html(this.followingPlaces.length);
    },

    // Set number of places being followed
    setSuggestedPlacesCount: function () {
        $("#suggestedPlacesCount").html(this.suggestedPlaces.length);
    },

    /**
    * This method should be invoked, whenever a search is complete
    * @params
    * searchResults - Array of search results
    * clearPreviousSuggestions - Flag (true/false) determining whether to clear previous search results or not
    */
    searchComplete: function (searchResults, clearPreviousResults) {
        var $resultsContainer = $("#searchResultsContainer");

        // clean up results container
        if (clearPreviousResults) $resultsContainer.html("");

        for (var i = 0, l = searchResults.length; i < l; i++) {
            $resultsContainer.append(this._createResultRow(searchResults[i], this._followCallback, this._unfollowCallback));
        }

        // Show container required for fetching more results
        $("#searchFurther").show();
    },

    /**
    * This method should be invoked, when suggestions for following places are found
    * @params
    * suggestions - Array of suggestions
    * clearPreviousSuggestions - Flag (true/false) determining whether to clear previous suggestions or not
    */
    suggetionsFound: function (suggestions, clearPreviousSuggestions) {
        var $resultsContainer = $("#suggestedContainer");

        // clean up results container
        if (clearPreviousSuggestions) $resultsContainer.html("");

        for (var i = 0, l = suggestions.length; i < l; i++) {
            $resultsContainer.append(this._createResultRow(suggestions[i], this._followCallback, this._unfollowCallback));
        }

        // Show container required for fetching more results
        $("#moreSuggestions").show();
    },

    // Create a search results row
    _createResultRow: function (resultDetails, followCallback, unfollowCallback) {
        var self = this;
        /*
        * Button Styles (Reference):
        *
        *
        * Following
        * <a href="#" class="hidden tiny button tiny-ex button-following radius right"><span class="icon-tick-following links-text "></span>&nbsp;Following</a>
        *
        * Follow
        * <a href="#" class="tiny button tiny-ex button-follow radius right"><span class="icon-plus "></span>&nbsp;&nbsp;&nbsp;Follow</a>
        *
        * Unfollow
        * <a href="#" class="hidden tiny button tiny-ex button-unfollow radius right"><span class="icon-cross-unfollow text-red"></span>&nbsp;&nbsp;&nbsp;unfollow</a>
        */
        var $row = utilities.createElement("<div/>", null, { 'class': "content-wrapper" });
        var $textContent = utilities.createElement("<div/>", $row, { 'class': "place" });

        utilities.createElement("<p/>", $textContent, { 'class': "place-title", 'html': resultDetails.location });
        utilities.createElement("<p/>", $textContent, { 'class': "place-info", 'html': '<span>' + resultDetails.roadtripsCount + ' Roadtrips</span><span> ' + resultDetails.followingCount + ' Following</span>' + (resultDetails.followersCount ? '<span>' + resultDetails.followersCount + ' Followers</span>' : '') });
        common.createFollowButton($row, { 'data': resultDetails, 'method': followCallback }, { 'data': resultDetails, 'method': unfollowCallback }, !resultDetails.isBeingFollowed, resultDetails.isBeingFollowed);

        return $row;
    },

    // Method to be invoked when Follow button is clicked
    _followCallback: function ($row, resultDetails) {
        // AJAX call goes HERE

        // If followed successfully
        this.followingPlaces.push(resultDetails);

        // set following places count again
        this.setFollowingPlacesCount();
    },

    // Method to be invoked when Follow button is clicked in Suggestions tab
    _suggestionsFollowCallback: function ($row, resultDetails) {
        // AJAX call goes HERE

        // If followed successfully
        this.followingPlaces.push(resultDetails);

        // set following places count again
        this.setFollowingPlacesCount();

        // Add this suggestion to following places tab
        //this.searchComplete([resultDetails], false);
    },

    // Method to be invoked when Unfollow button is clicked
    _suggestionsUnfollowCallback: function ($row, resultDetails) {
        var self = this;
        // AJAX call goes HERE

        // If un-followed successfully
        for (var i = 0, l = self.followingPlaces.length; i < l; i++) {
            if (self.followingPlaces[i]['id'] == resultDetails['id']) {
                self.followingPlaces.splice(i, 1);
                break;
            }
        }

        // set following places count again
        self.setFollowingPlacesCount();
    },

    // Fetch more results - Invoked when "Search Further" link below search results is clicked
    searchFurther: function () {
        /*var self = this;

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        var searchResults = [
        { 'id': 5, 'location': "NY, USA", 'roadtripsCount': "32", 'followingCount': "08", 'isBeingFollowed': false },
        { 'id': 6, 'location': "Gambia, Africa", 'roadtripsCount': "05", 'followingCount': "38", 'isBeingFollowed': false },
        { 'id': 7, 'location': "Paris, France", 'roadtripsCount': "2", 'followingCount': "14", 'isBeingFollowed': false },
        { 'id': 8, 'location': "Naples, Italy", 'roadtripsCount': "71", 'followingCount': "25", 'isBeingFollowed': false }
        ];

        // If results are fetched successfully
        self.searchComplete(searchResults, false);*/

        $(".elasticSearchTrigger").trigger("click");
    },

    // Fetch more suggestions - Invoked when "Show More Suggestions" link below suggestions is clicked
    moreSuggestions: function () {
        var self = this;

        // AJAX call goes HERE

        // SAMPLE DATA (remove for production use):
        var suggestions = [
        { 'id': 5, 'location': "NY, USA", 'roadtripsCount': "32", 'followingCount': "08", 'isBeingFollowed': false },
        { 'id': 6, 'location': "Gambia, Africa", 'roadtripsCount': "05", 'followingCount': "38", 'isBeingFollowed': false },
        { 'id': 7, 'location': "Paris, France", 'roadtripsCount': "2", 'followingCount': "14", 'isBeingFollowed': false },
        { 'id': 8, 'location': "Naples, Italy", 'roadtripsCount': "71", 'followingCount': "25", 'isBeingFollowed': false }
        ];

        // If results are fetched successfully
        self.suggetionsFound(suggestions, false);
    }
};

/*Page: profile_edit.html*/
var profile_edit = {
    initialize: function () {
        var self = this;

        // Quick methods
        this.toggleSocialIcons();

        // Enable uploading a photo
        $("#uploadPhotoBtn").on("click", function () {
            $("#photo").click();
        });
        $("#photo").on("change", function () {
            $("#profilePhotoForm").submit();
        });

        // Handle form submit for profile info
        $("#profileInfoForm").validate({
            highlight: function (element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function (form) {
                self.saveProfileInfo();

                return false;
            }
        });

        // Fetch & display existing user profile data
        this.fetchProfileInfo(this.displayProfileInfo);
    },

    // Fetch profile data from server
    fetchProfileInfo: function (fetchCallback) {
        // AJAX call goes HERE

        var profileData = {
            'name': "Tushar Agarwal",
            'location': "Bangalore",
            'weburl': "http://codeparallel.co",
            'bio': "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, voluptas ipsam sequi ipsum odit perspiciatis dolor in odio illo iusto quo reprehenderit facilis cum dignissimos nesciunt explicabo est hic dolores.",
            'social': {
                'fb': true,
                'twitter': true,
                'gplus': false
            }
        };

        // If request was successful
        fetchCallback && fetchCallback.apply(self, [profileData]);
    },

    // Fill in profile info in textboxes
    displayProfileInfo: function (profileData) {
        $.each(profileData, function (key, value) {
            key = $("#" + key);
            if (key.is("input[type='checkbox']")) {
                key.prop("checked", +value);
            } else {
                key.val(value);
            }
        });
    },

    // Save user profile data
    saveProfileInfo: function () {
        var profileData = utilities.formToObject("#profileInfoForm");

        // AJAX call goes HERE

        // If AJAX call is complete & data is saved successfully, appropriate success message can be displayed here
    },

    toggleSocialIcons: function () {
        $(".social-icon").on("mouseenter", function () {
            var index = $(this).index();

            if($(this).hasClass("_isDisabled")) {
                $(this).find("label").removeClass("no-connection");
                $(this).removeClass("_isDisabled");

                switch(index) {
                    case 0:
                        $(this).find("label").addClass("twitter");
                        break;

                    case 1:
                        $(this).find("label").addClass("facebook");
                        break;

                    case 2:
                        $(this).find("label").addClass("gplus");
                        break;
                }
            } else {
                $(this).addClass("_isDisabled");
                $(this).find("label:nth-child(1)").removeClass().addClass("button no-connection");
                $(this).find("label:nth-child(2)").removeClass().addClass("button check no-connection");
            }
        });
    }
};

var profile_changePassword = {
    initialize: function () {

        // Focus current password textbox by default
        $("#currentPassword").focus();

        // Handle form submit for profile info
        $("#changePasswordForm").validate({
            highlight: function (element, errorClass, validClass) {
                $(element).addClass("has-error");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass("has-error");
            },
            submitHandler: function (form) {
                self.sendChange();

                return false;
            }
        });
    },

    checkNewPasswords: function (newPassword1, newPassword2) {
        if (newPassword1 != "" && newPassword1 == newPassword2) return true;

        return false;
    },

    sendChange: function () {
        var passwords = utilities.formToObject("#changePasswordForm");

        if (passwords.currentPassword == "" || !this.checkNewPasswords(passwords.newPassword1, passwords.newPassword2)) {
            // New passwords do not match - display a message for the same

            return;
        }

        // AJAX call goes HERE

        // If AJAX call is complete & password is changed successfully, appropriate success message can be displayed here
    }
};

var profile_notifications = {
    initialize: function () {
        $("input[name='metric']").on("click", function () {
            
            $("input[name='metric']").parent().removeClass("active");

            if ($(this).is(':checked')) {
                $(".checkIcon").html("");

                $(this).parent().addClass("active");
                $(this).parent().find(".checkIcon").html('&nbsp;<span class="icon-tick-following"></span>');
            }
        });
    },

    // Save notification settings
    save: function () {
        var profileSettings = utilities.formToObject("#profileSettingsForm");

        // AJAX call goes HERE

        // If all settings are saved successfully, an appropriate success message can be triggered here
    }
};

var explore = {
    initialize: function () {
        var self = this;

        // Quick Methods
        common.createFollowButton("#followButtonContainer", null, null, true, false);
        $("#titleLocation").html("New Delhi, India");	// Set page title location
        this.getTrendingFeed(this.displayTrendingFeed);
        this.getRecentFeed(this.displayRecentFeed);
        this.getRelatedTags(this.displayRelatedTags);
        this.getRelatedLocations(this.displayRelatedLocations);


        // Lazy Methods
        this.map = utilities.createGMap(document.getElementById("map_canvas"));
        this.createZoomControl(this.map);
        this.createHelpControl(this.map);

        // Listen to map zoom
        // Note: This event is fired automatically on map load
        google.maps.event.addListener(this.map, "bounds_changed", function (ev) {
            var bounds = this.getBounds(),
                boundsData = {
                    'ne': bounds.getNorthEast(),
                    'sw': bounds.getSouthWest(),
                    'center': bounds.getCenter(),
                    'zoom': this.getZoom()
                };

                self.getMapLocations(boundsData, self.plotLocations);
        });

        // Temporary Listener (map click):
        /*var geocoder = new google.maps.Geocoder();
        google.maps.event.addListener(this.map, "click", function (ev) {
        	alert(ev.latLng.toString());
        	geocoder.geocode({'location':ev.latLng}, function (geocoderResult, geocoderStatus) {
        		if(geocoderStatus == google.maps.GeocoderStatus.OK) {
        			alert(geocoderResult[0].formatted_address);
        		}
        	});
		});*/
    },

    // fetch locations to plot on map
    getMapLocations: function (bounds, fetchCallback) {
        var self = this;

        // AJAX call goes here

        // SAMPLE DATA (remove for production use):
        var arrayOfLocations = [
    	{ 'id': 1, 'latitude': "40.979898", 'longitude': "104.765625", 'address': "Inner Mongolia, China" },
    	{ 'id': 2, 'latitude': "28.574874", 'longitude': "77.167968", 'address': "New Delhi, India" },
    	{ 'id': 3, 'latitude': "22.431340", 'longitude': "86.704101", 'address': "Jharkhand, India" },
    	{ 'id': 4, 'latitude': "31.615965", 'longitude': "1.362304", 'address': "El Bayadh, Algeria" },
    	{ 'id': 5, 'latitude': "64.774125", 'longitude': "-17.885742", 'address': "Vatnajoekull National Park, Iceland" },
    	{ 'id': 6, 'latitude': "36.738884", 'longitude': "-120.585937", 'address': "Firebaugh, CA, USA" },
    	{ 'id': 7, 'latitude': "-1.581830", 'longitude': "-63.808593", 'address': "Barcelos, Amazonas, Brazil" }
        ];

        // If list of locations is fetched successfully, send data in callback

        if (fetchCallback) fetchCallback.apply(self, [arrayOfLocations]);
    },

    plotLocations: function (mapLocations) {
        var marker,
    	i = 0,
    	l = mapLocations.length,
    	latlng,
    	self = this;

        // Initially remove all markers
        this.removeAllMarkers();

        for (; i < l; i++) {
            marker = this.createWhiteMarker(this.map);
            marker.setIcon(Markers.whiteIcon);
            marker.setPosition(new google.maps.LatLng(mapLocations[i].latitude, mapLocations[i].longitude));
            marker.clickFlag = 0;

            google.maps.event.addListener(marker, "click", (function (mapLocation) {
                return function () {
                    if($("#map-help-text").css("display") != "none") $("#map-help-text").fadeOut(1200);

                    if(self.currentlyClickedMarker) {
                        self.hideInfoWindow();
                        self.currentlyClickedMarker.setIcon(Markers.whiteIcon);
                    }

                    this.setIcon(Markers.redIcon);
                    self.showInfoWindow(this, mapLocation);

                    self.currentlyClickedMarker = this;
                }
            })(mapLocations[i]));
            
            // Add all newly created markers to global markers array
            this.markers.push(marker);
        }
    },

    infoWindow: null,

    markers: [],

    currentlyClickedMarker: null,

    // Remove all markers from map and clear array
    removeAllMarkers: function () {
        for(var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }

        this.markers = [];
    },

    showInfoWindow: function (marker, mapLocation) {
        this.infoWindow = new Ex_InfoBox({
            'latLng': marker.getPosition(),
            'content': mapLocation.address
        }, this.map);
    },

    hideInfoWindow: function () {
        this.infoWindow.setMap(null);
    },

    createZoomControl: function (map) {
        // Create a div to hold the control.
        var controlDiv = document.createElement('div');

        // Set CSS styles for the DIV containing the control
        // Setting padding to 45 px will offset the control
        // from the edge of the map.
        controlDiv.style.padding = '45px';

        var zoomInHelper = this.createZoomHelper("+");
        google.maps.event.addDomListener(zoomInHelper, 'click', function () {
            map.setZoom(map.getZoom() + 1);
        });
        controlDiv.appendChild(zoomInHelper);

        var zoomOutHelper = this.createZoomHelper("-");
        google.maps.event.addDomListener(zoomOutHelper, 'click', function () {
            map.setZoom(map.getZoom() - 1);
        });
        controlDiv.appendChild(zoomOutHelper);

        // We don't really need to set an index value here, but
        // this would be how you do it. Note that we set this
        // value as a property of the DIV itself.
        controlDiv.index = 1;

        // Add the control to the map at a designated control position
        // by pushing it on the position's array. This code will
        // implicitly add the control to the DOM, through the Map
        // object. You should not attach the control manually.
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(controlDiv);
    },

    createZoomHelper: function (text) {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.title = 'Zoom In';
        controlUI.className = "zoomControl";

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.fontSize = '24px';
        controlText.style.fontFamily = 'Arial';
        controlText.style.fontWeight = '700';
        controlText.style.lineHeight = '32px';
        controlText.style.color = 'white';
        controlText.innerHTML = text;
        controlUI.appendChild(controlText);

        return controlUI;
    },

    createHelpControl: function (map) {
        // Create a div to hold the control.
        var controlDiv = document.createElement('div');

        // Set CSS styles for the DIV containing the control
        // Setting padding to 45 px will offset the control
        // from the edge of the map.
        controlDiv.style.padding = '57px';

        var htBlock = this.createHelpTextBlock("Select region of your interest. Zoom into places to discover more");
        controlDiv.appendChild(htBlock);

        // We don't really need to set an index value here, but
        // this would be how you do it. Note that we set this
        // value as a property of the DIV itself.
        controlDiv.index = 2;

        // Add the control to the map at a designated control position
        // by pushing it on the position's array. This code will
        // implicitly add the control to the DOM, through the Map
        // object. You should not attach the control manually.
        map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(controlDiv);
    },

    createHelpTextBlock: function (text) {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.className = "helpText clearfix";
        controlUI.id = "map-help-text";

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.innerHTML = text;
        controlText.className = "help-text";
        controlUI.appendChild(controlText);

        // Set CSS for control tooltip
        var controlTooltip = document.createElement("a");
        controlTooltip.innerHTML = "?";
        controlTooltip.className = "help-tooltip-helper";
        controlUI.appendChild(controlTooltip);

        return controlUI;
    },

    createWhiteMarker: function (map) {
        return new google.maps.Marker({
            'draggable': false,
            'map': map
        });
    },

    getTrendingFeed: function (fetchCallback) {
        var self = this;

        // AJAX call goes here

        // SAMPLE DATA (remove for production use):
        var arrayOfFeeds = [{
            'userPic': "../images/feed_photo.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "../images/coffee-beans.jpg",
            'mapLink': "",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        },
        {
            'userPic': "../images/feed_photo.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "../images/coffee-beans.jpg",
            'mapLink': "",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        }];

        // If list of feeds is fetched successfully, send data in callback

        if (fetchCallback) fetchCallback.apply(self, [arrayOfFeeds]);
    },

    getRecentFeed: function (fetchCallback) {
        var self = this;

        // AJAX call goes here

        // SAMPLE DATA (remove for production use):
        var arrayOfFeeds = [{
            'userPic': "../images/feed_photo.jpg",
            'userName': "Dave Smith",
            'minsAgo': "23",
            'noViews': "34",
            'tripPic': "../images/coffee-beans.jpg",
            'mapLink': "",
            'tripName': "Summer retreat trip in Deer Vallery, Utah",
            'tripDesc': "Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.Winchester has always been on my mind as it used to the Capital of England and has one of the largest gothic cathedrals in the world. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford. We had time in hand so thought of including Rochester in the itinerary and stay the night in a lovely guest house in small Guildford.",
            'noMilestones': "4",
            'noMoments': "8",
            'noPhotos': "14",
            'likes': "8",
            'comments': "3"
        }];

        // If list of feeds is fetched successfully, send data in callback

        if (fetchCallback) fetchCallback.apply(self, [arrayOfFeeds]);
    },

    displayTrendingFeed: function (listOfFeed) {
        me_trips.printFeeds(listOfFeed, "#trendingFeed");
    },

    displayRecentFeed: function (listOfFeed) {
        me_trips.printFeeds(listOfFeed, "#recentFeed", "oneColFeed");
    },

    getRelatedTags: function (fetchCallback) {
        var self = this;

        // AJAX call goes here

        // SAMPLE DATA (remove for production use):
        var arrayOfTags = [
    	{ 'id': 1, 'tagHref': "#", 'tagText': "#India" },
    	{ 'id': 2, 'tagHref': "#", 'tagText': "#Indian-food" },
    	{ 'id': 3, 'tagHref': "#", 'tagText': "#culture" },
    	{ 'id': 4, 'tagHref': "#", 'tagText': "#red-fort" },
    	{ 'id': 5, 'tagHref': "#", 'tagText': "#example-tag" },
    	{ 'id': 6, 'tagHref': "#", 'tagText': "#roadmojo" },
    	{ 'id': 7, 'tagHref': "#", 'tagText': "#travel" }
        ];

        // If list of tags is fetched successfully, send data in callback

        if (fetchCallback) fetchCallback.apply(self, [arrayOfTags]);
    },

    getRelatedLocations: function (fetchCallback) {
        var self = this;

        // AJAX call goes here

        // SAMPLE DATA (remove for production use):
        var arrayOfLocations = [
    	{ 'id': 1, 'tagHref': "#", 'tagText': "Leh" },
    	{ 'id': 2, 'tagHref': "#", 'tagText': "Chandigarh" },
    	{ 'id': 3, 'tagHref': "#", 'tagText': "Agra" },
    	{ 'id': 4, 'tagHref': "#", 'tagText': "Dharamshala" },
    	{ 'id': 5, 'tagHref': "#", 'tagText': "Kerala" },
    	{ 'id': 6, 'tagHref': "#", 'tagText': "Bangalore" },
    	{ 'id': 7, 'tagHref': "#", 'tagText': "Example location name" }
        ];

        // If list of locations is fetched successfully, send data in callback

        if (fetchCallback) fetchCallback.apply(self, [arrayOfLocations]);
    },

    displayRelatedTags: function (listOfTags) {
        var i = 0,
    		l = listOfTags.length,
    		$tagsContainer = $("#relatedTagsContainer");

        for (; i < l; i++) {
            $tagsContainer.append(this.createTag(listOfTags[i]));
        }
    },

    displayRelatedLocations: function (listOfLocations) {
        var i = 0,
    		l = listOfLocations.length,
    		$tagsContainer = $("#relatedLocationsContainer");

        for (; i < l; i++) {
            $tagsContainer.append(this.createTag(listOfLocations[i]));
        }
    },

    createTag: function (tagDetails) {
        // <span class="button radius tag">#India</span>
        var $tag = utilities.createElement("<a/>", null, { 'href': tagDetails.tagHref });
        utilities.createElement("<span/>", $tag, { 'class': "button radius tag", 'html': tagDetails.tagText });

        return $tag;
    }
};

var common = {
    initialize: function () {
        this.confirms();
        this.trip();
        this.tooltip();
        this.g_search();
        this.site_header();
    },

    // reduce header height on scroll
    site_header: function () {
        var self = this;

        $(document).scroll(function (ev) {
            var $aside = $("aside");

            if ($(document).scrollTop() > 100) {
                $(".top-bar-container.sticky").addClass("top-bar-small-height");
                $(".top-bar .name").addClass("top-bar-small-height");
                $(".top-bar").addClass("top-bar-small-height top-bar-small-lheight");
                $(".top-bar .top-bar-section > ul > li > a, .top-bar-container.sticky  ul.title-area > li > h1 > a").addClass("top-bar-small-lheight");
                $(".top-bar .top-bar-section ul .account-img").addClass("top-bar-small-margin");
                self.searchOverlay.css("top", "50px");

                if($aside.length && $aside.offset().top - $(window).scrollTop() <= 50) $aside.css({ 'position': "fixed", 'top': "65px" });
            } else {
                $(".top-bar .name").removeClass("top-bar-small-height");
                $(".top-bar-container.sticky").removeClass("top-bar-small-height");
                $(".top-bar").removeClass("top-bar-small-height top-bar-small-lheight");
                $(".top-bar .top-bar-section > ul > li > a, .top-bar-container.sticky ul.title-area > li > h1 > a").removeClass("top-bar-small-lheight");
                $(".top-bar .top-bar-section ul .account-img").removeClass("top-bar-small-margin");
                self.searchOverlay.css("top", "75px");

                $aside.length && $aside.css("position", "static");
            }
        });
    },

    // confirm & delete boxes
    confirms: function () {
        var self = this;

        // default configuration of confirmation modal
        $(".confirm").foundation("reveal", {
            animation: 'fadeAndFade',
            close_on_background_click: false
        });

        // add "CANCEL" handler to confirmation modal
        $(".cancel-confirm").on("click", function () {
            $(".confirm").foundation('reveal', 'close');
        });

        // add "DELETE" handler to confirmation modal
        $(".delete-confirm").on("click", function (ev) {
            // Send AJAX request to delete something
            // AJAX call goes HERE

            /* If request was sucessful: */
            // check & execute callback
            self._confirmsDelete && self._confirmsDelete();
            // Finally, close modal
            $(".confirm").foundation('reveal', 'close');
        });
    },

    // confirms-delete callback function (called when Delete button is pushed on a confirm dialog box)
    _confirmsDelete: null,

    // all trip commons are handled here
    trip: function () {
        // handle like click
        $(".like").on("click", function () {
            if ($(this).hasClass("nav-text")) {
                $(this).removeClass("nav-text");

                /**Liked**/
                // AJAX call: like this trip
            } else {
                $(this).addClass("nav-text");

                /**Un-Liked**/
                // AJAX call: un-like this trip
            }
        });

        // handle comment click
        $(".comment").on("click", function () {
            window.location = "#";
        });

        // Display profile pics of users - sidebar
        // Fetch & display
        this._fetchUsersPics(this._displayUsersPics);
    },

    // Fetch pics of users for sidebar
    _fetchUsersPics: function (fetchCallback) {
        var self = this;

        // SAMPLE DATA (remove for production use):
        var arrayOfImages = [
        "http://lorempixel.com/47/47/people/1",
        "http://lorempixel.com/47/47/people/2",
        "http://lorempixel.com/47/47/people/3",
        "http://lorempixel.com/47/47/people/4",
        "http://lorempixel.com/47/47/people/5",
        "http://lorempixel.com/47/47/people/6",
        "http://lorempixel.com/47/47/people/7",
        "http://lorempixel.com/47/47/people/8",
        "http://lorempixel.com/47/47/people/9",
        "http://lorempixel.com/47/47/people/10",
        "http://lorempixel.com/47/47/people/7",
        "http://lorempixel.com/47/47/people/8",
        "http://lorempixel.com/47/47/people/9",
        "http://lorempixel.com/47/47/people/10",
        "http://lorempixel.com/47/47/people/7"
        ];

        // Fetch img paths of all user photos
        // AJAX call goes HERE

        // If request completes successfully:

        // Execute callback - with array of img paths
        fetchCallback && fetchCallback.apply(self, [arrayOfImages]);
    },

    // Display pics of users in sidebar
    _displayUsersPics: function (arrayOfImages) {
        //debugger;
        var picsInARow = 6,
        maxRows = 2,
        imgCount = arrayOfImages.length,
        $imagesContainer = $(".profile-pics"),
        addPic = function (imgPath) {
            utilities.createElement("<img/>", $imagesContainer, { 'src': imgPath, 'alt': "" });
        },
        i = 0;

        if (imgCount <= picsInARow) {
            // Total number of followers is less than or equal to total number of pics required in a row

            // loop through imgCount and display images
            for (; i < imgCount; i++) {
                addPic(arrayOfImages[i]);
            }
        } else if (imgCount > picsInARow) {
            // Total number of followers is greater than total number of pics required all rows

            // calculate number of images that should be displayed and number of remaining images
            var remainingCount = (imgCount + 1) % picsInARow,
            displayCount = imgCount - remainingCount,
            rows = Math.ceil(displayCount / picsInARow),
            rCount = 0;

            // If number of rows that are going to be displayed exceed the maximum number of allowed rows
            if (rows > maxRows) {
                var excessPics = (rows - maxRows) * picsInARow;
                displayCount -= excessPics;
                remainingCount += excessPics;
            }

            // loop through picsInARow and display images
            for (; rCount < rows; rCount++) {
                for (; i < displayCount; i++) {
                    addPic(arrayOfImages[i]);
                }
            }

            // Display number of excess images
            utilities.createElement("<span/>", $imagesContainer, { 'class': "more left text-center", 'html': "+" + remainingCount });
        }
    },

    // custom tooltip
    tooltip: function () {
        $(document).foundation({
            tooltips: {
                selector: '.has-tip',
                additional_inheritable_classes: [],
                tooltip_class: '.tooltip',
                touch_close_text: 'tap to close',
                disable_for_touch: false,
                tip_template: function (selector, content) {
                    return '<span data-selector="' + selector + '" class="'
    				+ Foundation.libs.tooltips.settings.tooltipClass.substring(1)
    				+ '">' + content + '<span class="nub"></span></span>';
                }
            }
        });
    },

    g_search: function () {
        var self = this;

        if (!this.searchOverlay) this.searchOverlay = this.elasticSearch();

        $(".elasticSearchTrigger").on("click", function () {
            if(self.searchOverlayState == 0) {
                // Goto top of page
                utilities.scrollToTop();

                // Set overflow of body to be hidden - fix for not scrolling
                //$("body").css("overflow", "hidden");

                // Add search overlay to body
                $("body").append(self.searchOverlay);
                self.searchOverlay.slideDown(400);

                // Change search icon to cross
                var searchIcon = $(this).find(".icon-search-default");
                searchIcon.removeClass("icon-search-default");
                searchIcon.addClass("icon-search-close");

                // Focus on search textbox for quick typing
                self.searchOverlay.find(".place-search").focus();
            } else {
                // Change cross icon to search icon again
                var searchIcon = $(this).find(".icon-search-close");
                searchIcon.removeClass("icon-search-close");
                searchIcon.addClass("icon-search-default");

                self.searchOverlay.slideUp(400, function () {
                    // Remove search overlay from body
                    self.searchOverlay.remove();

                    // Set overflow of body to be visible
                    //$("body").css("overflow", "visible");
                });
            }
            self.searchOverlayState ^= 1;
        });
    },

    searchOverlayState: 0,

    searchOverlay: null,

    // Create elastic search box/overlay
    elasticSearch: function () {
        var self = this;

        var $searchWrap = utilities.createElement("<div/>", null, { 'class': "search-wrapper hidden" });
        var $searchHeading = utilities.createElement("<div/>", $searchWrap, { 'class': "search-heading" });
        var $col = utilities.createElement("<div/>", null, { 'class': "medium-12 large-12 column no-padding-left" }).appendTo(utilities.createElement("<div/>", $searchHeading, { 'class': "row" }));
        var $searchInput = utilities.createElement("<input/>", $col, { 'class': "place-search main", 'type': "search", 'placeholder': "Search for places..." });
        $searchInput.on("keyup", Foundation.utils.throttle(function (e) {
            self.getSearchResults($(this).val(), $searchWrap, self.displaySearchResults);
        }, 300));

        utilities.createElement("<div/>", null, { 'class': "search-content" }).appendTo(utilities.createElement("<div/>", $searchWrap, { 'class': "search-container" }));

        return $searchWrap;
    },

    getSearchResults: function (searchKeys, $search, fetchCallback) {
        var self = this;

        // SAMPLE DATA (remove for production use):
        /*var arrayOfResults = [
        	{ 'id': 1, 'type': 1, 'locationText': "New Delhi, India", 'additionalText': "", 'roadtripsCount': 32, 'followersCount': 34 },
        	{ 'id': 2, 'type': 1, 'locationText': "Delhi, Louisiana", 'additionalText': "", 'roadtripsCount': 32, 'followersCount': 34 },
        	{ 'id': 3, 'type': 2, 'locationText': "Delhi, Leh exploration", 'additionalText': "by Username, Delhi to Leh", 'roadtripsCount': 32, 'followersCount': 34 },
        	{ 'id': 4, 'type': 2, 'locationText': "Example result title on hover", 'additionalText': "by Username, Delhi to Chandigarh", 'roadtripsCount': 32, 'followersCount': 34 },
        	{ 'id': 5, 'type': 2, 'locationText': "Example roadtrip with delhi as milestone", 'additionalText': "by Username, Delhi to Pathankot", 'roadtripsCount': 32, 'followersCount': 34 }
        ];*/

        // Fetch img paths of all user photos
        $.ajax({
            type: "POST",
            async: true,
            data: searchKeys,
            url: "",
            success: function (data, textStatus, jqXHR) {
                // data array should be received here in JSON format

                if (fetchCallback) fetchCallback.apply(self, [$search.find('.search-content'), data]);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("unable to fetch list of search results " + errorThrown);
            }
        });
    },

    displaySearchResults: function ($searchResultsContainer, listOfSearchResults) {
        var i = 0,
    		l = listOfSearchResults.length;

        $searchResultsContainer.html("");

        for (; i < l; i++) {
            $searchResultsContainer.append(this.createSearchResult(listOfSearchResults[i]));
        }
    },

    // Create an individual search result row
    createSearchResult: function (resultDetails) {
        var $contentWrapper = utilities.createElement("<div/>", null, { 'class': "row content-wrapper" })
        var $contentInnerWrapper = utilities.createElement("<div/>", $contentWrapper, { 'class': "medium-7 large-7 column content-inner-wrapper" });
        utilities.createElement("<div/>", $contentInnerWrapper, { 'class': "search-place-result left", 'html': resultDetails.locationText });
        utilities.createElement("<div/>", $contentInnerWrapper, { 'class': "search-place-result-by", 'html': "&nbsp;" + resultDetails.additionalText });

        $col = utilities.createElement("<div/>", $contentWrapper, { 'class': "medium-5 large-5 column" });
        $col = utilities.createElement("<div/>", null, { 'class': "large-12 column clearfix" }).appendTo(utilities.createElement("<div/>", $col, { 'class': "row" }));
        switch (resultDetails.type) {
            case 1:
                this.createFollowButton($col, resultDetails.id, resultDetails.id, true, false);
                utilities.createElement("<div/>", $col, { 'class': "place-info text-right right", 'html': resultDetails.roadtripsCount + " Roadtrips, " + resultDetails.followersCount + " Followers" });
                break;

            case 2:
                var $rightContainer = utilities.createElement("<div/>", $col, { 'class': "roadtrip-view text-right" });
                utilities.createElement("<a/>", $rightContainer, { 'href': "#", 'class': "roadtrip-view-link", 'html': 'View Roadtrip <span class="gt">&gt;</span>' });
                break;
        }

        return $contentWrapper;
    },

    // The standard follow button
    createFollowButton: function ($container, followCallbackData, unfollowCallbackData, showFollow, showFollowing) {
        /*
        * Button Styles (Reference):
        *
        *
        * Following
        * <a href="#" class="hidden tiny button tiny-ex button-following radius right"><span class="icon-tick-following links-text "></span>&nbsp;Following</a>
        *
        * Follow
        * <a href="#" class="tiny button tiny-ex button-follow radius right"><span class="icon-plus "></span>&nbsp;&nbsp;&nbsp;Follow</a>
        *
        * Unfollow
        * <a href="#" class="hidden tiny button tiny-ex button-unfollow radius right"><span class="icon-cross-unfollow text-red"></span>&nbsp;&nbsp;&nbsp;unfollow</a>
        */
        var self = this;

        $container = $($container);

        // Follow button - Green with + icon
        var $followButton = utilities.createElement("<a/>", $container, { 'href': "#", 'class': (showFollow ? "" : "hidden ") + "tiny button tiny-ex button-follow radius right", 'html': '<span class="icon-plus "></span>&nbsp;&nbsp;&nbsp;Follow' });
        $followButton.on("click", function (ev) {
            ev.preventDefault();

            // Hide the Follow button & show the Following/Unfollow button
            $(this).hide();
            $followingButton.fadeIn();

            // Method to be called when Follow button is clicked
            self.followCallback.apply(self, [followCallbackData]);
        });

        var $followingButton = utilities.createElement("<a/>", $container, { 'href': "#", 'class': (showFollowing ? "" : "hidden ") + "tiny button tiny-ex button-following radius right", 'html': '<span class="icon-tick-following links-text "></span>&nbsp;Following' });
        $followingButton.on("mouseover", function () {
            // On mouseover, 'Following' Button changes to 'Unfollow' Button
            $(this).html('<span class="icon-cross-unfollow text-red"></span>&nbsp;&nbsp;Unfollow').removeClass('button-following').addClass('button-unfollow');

            // Add method to handle Unfollow button click
            $(this).on("click", function (ev) {
                ev.preventDefault();

                // Hide the Following/Unfollow button & show the Follow button
                $(this).hide();
                $followButton.show();

                // Method to be called when Unfollow button is clicked
                self.unfollowCallback.apply(self, [unfollowCallbackData]);
            });
        });
        $followingButton.on("mouseout", function () {
            // On mouseout, 'Unfollow' Button changes to 'Following' Button
            $(this).html('<span class="icon-tick-following links-text "></span>&nbsp;Following').removeClass('button-unfollow').addClass('button-following');

            // Remove Unfollow button click handler
            $(this).off("click");
        });
    },

    followCallback: function (callbackData) {
        // AJAX call to follow goes here
        callbackData.method && callbackData.method();
    },

    unfollowCallback: function (callbackData) {
        // AJAX call to unfollow goes here
        callbackData.method && callbackData.method();
    }
};