class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :location
      t.string :webburl
      t.string :bio

      t.timestamps
    end
  end
end
